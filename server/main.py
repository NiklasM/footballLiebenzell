#!/usr/bin/env python3.6
# coding=utf-8
import logging
import threading
import sys
from client import HumanClient, simpleClient
from game import Game
from gui import Gui
from network import ServerThread


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format="%(levelname)s %(message)s", stream=sys.stdout)
    server = ServerThread()
    game = Game(server)
    humanList = []
    humanList.append(HumanClient("v", "i", "a", "u", "p"))
    humanList.append(HumanClient("Up", "Down", "Right", "Left", "Return"))
    gui = Gui(game, humanList)
    server.start(game)
    
    humanList[0].start()
    humanList[1].start()
    threading.Thread(daemon=True, target=simpleClient).start()
    threading.Thread(daemon=True, target=simpleClient).start()
    threading.Thread(daemon=True, target=simpleClient).start()
    threading.Thread(daemon=True, target=simpleClient).start()
    
    logging.info("starting GUI")
    gui.start()
    logging.info("GUI closed")