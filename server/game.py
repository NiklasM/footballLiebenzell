#!/usr/bin/env python3.6
# coding=utf-8

import logging
import threading

from event import Event
from settings import *
import time
from entity import *
from geometry import *
from pprint import pprint



class Game:
    def __init__(self, server):
        self.lock = threading.RLock()
        self.server = server
        self.players = dict()
        self.goalAmount = [0,0]
        self.teams = [set(),set()]
        self.ball = Entity(Point(WIDTH/2, HEIGHT/2), BALL_SIZE, BALL_FRICTION)
        self.canvas = None
    
    
    def step(self):
        for entity in self.iterateEntities():
            entity.move()
        self.kickAll()
        self.checkCollisions()
        self.manageGoal()
    
    
    def manageGoal(self):
        goalEvent = None
        if (self.ball.pos.x < 0):
            goalEvent = Event({"event":"goal", "team":0})
        elif (self.ball.pos.x > WIDTH):
            goalEvent = Event({"event":"goal", "team":1})
        
        if goalEvent is not None:
            self.server.broadcast(goalEvent)
            self.goalAmount[goalEvent.team] += 1
            
            #redistribute teams?
            
            for id in self.players:
                self.placePlayer(id)
            self.ball = Entity(Point(WIDTH/2, HEIGHT/2), BALL_SIZE, BALL_FRICTION)
            self.redraw()
        
        eventEntries = {"event":"gameState"}
        eventEntries.update(self.reprJSON())
        gameState = Event(eventEntries)
        self.server.broadcast(gameState)
    
    
    def removePlayer(self, id) -> bool:  #true if actual Player was removed
        if self.players.pop(id) == None:
            return False
        if id in self.teams[0]:
            self.teams[0].remove(id)
        elif id in self.teams[1]:
            self.teams[1].remove(id)
        return True
    
    
    def addPlayer(self, id, player) -> int:
        self.players[id] = player
        team = 1
        if len(self.teams[0]) <= len(self.teams[1]):
            team = 0
        self.teams[team].add(id)
        self.placePlayer(id)
        self.redraw()
        return team
    
    
    def placePlayer(self, id):
        team = 0
        if id in self.teams[1]:
            team = 1
        if team == 0:
            self.players[id].pos.x = WIDTH/4
        else:
            self.players[id].pos.x = 3*WIDTH/4
        
        teamSize = len(self.teams[team])+1
        numberInTeam = 1
        for referenceId in self.teams[team]:
            if referenceId == id:
                break
            numberInTeam+=1

        self.players[id].pos.y = HEIGHT*(numberInTeam * 0.75/teamSize + 0.125)
        self.players[id].speed = Vector(0,0)
    
    
    def iterateEntities(self):
        for player in self.players.values():
            yield player
        yield self.ball
    
    
    def kickAll(self):
        for player in self.players.values():
            if player.kicking:
                player.kicking = False
                dist = player.pos - self.ball.pos
                if dist.length() >= player.size + self.ball.size + SHOOT_DISTANCE or dist.length2() < 0.1:
                    continue
                acceleration = dist.normalize() * -SHOOT_FORCE
                self.ball.speed += acceleration
    
    
    def checkCollisions(self):
        leftout = -WIDTH*GOAL_DEPTH
        rightout = WIDTH*(1+GOAL_DEPTH)
        goalup = HEIGHT*(1-GOAL_SIZE)/2
        goaldown = goalup+(HEIGHT*GOAL_SIZE)
        
        
        #entity-wall collision
        for entity in self.iterateEntities():
            def collideLine(pos :float, xaxis :bool, upLeft :bool, bound1 :float=None, bound2 :float=None):
                upMul = -1
                if upLeft: upMul = 1
                moveVector = Vector(0,1)
                antiOperand = entity.pos.x
                operand = entity.pos.y
                if xaxis:
                    moveVector = Vector(1,0)
                    antiOperand = entity.pos.y
                    operand = entity.pos.x
                if upMul*operand-entity.size < pos*upMul and (bound1 is None or antiOperand > bound1) and (bound2 is None or antiOperand < bound2):
                    entity.move(moveVector * (pos + (entity.size*upMul) - operand))
                    if xaxis: entity.speed.x *= -1
                    else: entity.speed.y *= -1
                
            def collidePoint(p :Point):
                dist = entity.pos - p
                if dist.length() > entity.size or dist.length2() < 0.1:
                    return
                backLength = (entity.size - dist.length()) / dist.length()
                entity.move(dist*backLength)
            
            #goalposts
            collidePoint(Point(0,goalup))
            collidePoint(Point(0,goaldown))
            collidePoint(Point(WIDTH,goalup))
            collidePoint(Point(WIDTH,goaldown))
            
            #up and down
            collideLine(0, False, True)
            collideLine(HEIGHT, False, False)
            #left and right
            collideLine(WIDTH, True, False, None, goalup)
            collideLine(rightout, True, False, goalup, goaldown)
            collideLine(WIDTH, True, False, goaldown, None)
            collideLine(0, True, True, None, goalup)
            collideLine(leftout, True, True, goalup, goaldown)
            collideLine(0, True, True, goaldown, None)
            #up and down in goal
            collideLine(goalup, False, True, WIDTH, None)
            collideLine(goaldown, False, False, WIDTH, None)
            collideLine(goalup, False, True, None, 0)
            collideLine(goaldown, False, False, None, 0)
        
        #player-player collision
        for (id1, player1) in self.players.items():
            for (id2, player2) in self.players.items():
                if id1 == id2:
                    continue
                dist = player1.pos - player2.pos
                if dist.length() > player1.size + player2.size or dist.length2() < 0.1:
                    continue
                backLength = (player1.size + player2.size - dist.length()) / dist.length()
                player1.move(dist * backLength)
                player2.move(dist * -backLength)
                player1.speed += dist * backLength * COLLISION_FORCE
                player2.speed += dist * -backLength * COLLISION_FORCE
        
        #player-ball collision
        for player in self.players.values():
            dist = player.pos - self.ball.pos
            if dist.length() >= player.size + self.ball.size or dist.length2() < 0.1:
                continue
            backLength = (player.size + self.ball.size - dist.length()) / dist.length()
            player.move(dist * backLength * 0.2)
            self.ball.move(dist * -backLength * 1.8)
            player.speed += dist * backLength * 0.2 * COLLISION_FORCE
            self.ball.speed += dist * -backLength * 1.8 * COLLISION_FORCE
    
    
    def redraw(self, canvas=None):
        if canvas is not None:
            self.canvas = canvas
        if self.canvas is None:
            return
        logging.info("redrawing")
        self.canvas.delete("field")
        self.canvas.delete("entity")
    
        margin = 20
        w = self.canvas.width - margin * 2
        h = self.canvas.height - margin * 2
    
        trans = Transformation(margin, margin)
        if (w / h > (WIDTH + GOAL_DEPTH * 2 * WIDTH) / HEIGHT):  # height too small
            trans.scaley = h / HEIGHT
            trans.scalex = trans.scaley
            trans.addx += (w - ((WIDTH + GOAL_DEPTH * 2 * WIDTH) * trans.scalex)) / 2
        else:  # width too small
            trans.scalex = w / (WIDTH + GOAL_DEPTH * 2 * WIDTH)
            trans.scaley = trans.scalex
            trans.addy += (h - (HEIGHT * trans.scaley)) / 2
        trans.addx += GOAL_DEPTH * WIDTH * trans.scalex
    
        line = []
        line.append(Point(0, 0))
        line.append(Point(WIDTH, 0))
        line.append(Point(WIDTH, HEIGHT * (1 - GOAL_SIZE) / 2))
        line.append(Point(WIDTH * (1 + GOAL_DEPTH), HEIGHT * (1 - GOAL_SIZE) / 2))
        line.append(Point(WIDTH * (1 + GOAL_DEPTH), HEIGHT * ((1 - GOAL_SIZE) / 2 + GOAL_SIZE)))
        line.append(Point(WIDTH, HEIGHT * ((1 - GOAL_SIZE) / 2 + GOAL_SIZE)))
        line.append(Point(WIDTH, HEIGHT))
        line.append(Point(0, HEIGHT))
        line.append(Point(0, HEIGHT * ((1 - GOAL_SIZE) / 2 + GOAL_SIZE)))
        line.append(Point(-GOAL_DEPTH * WIDTH, HEIGHT * ((1 - GOAL_SIZE) / 2 + GOAL_SIZE)))
        line.append(Point(-GOAL_DEPTH * WIDTH, HEIGHT * (1 - GOAL_SIZE) / 2))
        line.append(Point(0, HEIGHT * (1 - GOAL_SIZE) / 2))
    
        for i in range(len(line)):
            line[i] = line[i].transform(trans)
        for i in range(len(line)):
            self.canvas.create_line(line[i - 1].x, line[i - 1].y, line[i].x, line[i].y, tags="field", width=5, fill="white")
    
        for entity in self.iterateEntities():
            entity.redraw(self.canvas, trans)
    
    
    def reprJSON(self):
        rep = dict()
        playerList = []
        for (id, player) in self.players.items():
            playerRep = player.reprJSON()
            playerRep["id"] = id
            playerList.append(playerRep)
        rep["players"] = playerList
        rep["ball"] = self.ball.reprJSON()
        return rep

    
    
    
    
        
        