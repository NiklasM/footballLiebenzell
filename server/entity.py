#!/usr/bin/env python3.6
# coding=utf-8

import logging
import math
import sys
from settings import *
from geometry import *


class Entity:
    def __init__(self, pos :Point, size :int, friction :float, name :str = ""):
        self.pos :Point = pos
        self.speed :Vector = Vector(0,0)
        self.accel :Vector = Vector(0,0)
        self.size :int = size
        self.friction :float = friction
        self.name :str = name
        self.kicking :bool = False
        self.canvas = None
        self.transformation = None
        self.graphics = None
    
    def changeAccel(self, accel :Vector):
        self.accel = accel.cut()
    
    def move(self, direction=None):  #if direction not set, use self.speed
        if direction is None:
            self.speed *= 1-self.friction
            self.speed += self.accel * MAX_ACCEL
            direction = self.speed
        self.pos += direction
        
        if self.transformation is not None and self.canvas is not None and self.graphics is not None:
            movement = direction.transform(self.transformation)
            self.canvas.move(self.graphics, movement.x, movement.y)
            
        
    
    def redraw(self, canvas, trans):
        self.canvas = canvas
        self.transformation = trans
        upperLeft = (self.pos - Vector(self.size, self.size)).transform(trans)
        lowerRight = (self.pos + Vector(self.size, self.size)).transform(trans)
        coord = upperLeft.x, upperLeft.y, lowerRight.x, lowerRight.y
        self.graphics = canvas.create_oval(coord , fill="red", tag="entity")
    
    def reprJSON(self):
        rep = dict()
        rep["x"] = self.pos.x
        rep["y"] = self.pos.y
        rep["sx"] = self.speed.x
        rep["sy"] = self.speed.y
        return rep



if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format="%(levelname)s %(message)s", stream=sys.stdout)
    
    entity = Entity(Point(0,0),PLAYER_SIZE)
    entity.changeAccel(Vector(1,1))
    entity.move()
    entity.move()
    logging.info(entity.pos.x)
    passed = entity.pos.x > 0.7 and entity.pos.x < 0.71
    if not passed:
        logging.error("Player test failed!")
    else:
        logging.info("Player test successful")
    
    