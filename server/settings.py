#!/usr/bin/env python3.6
# coding=utf-8

#Network
PORT = 60001

#sizes
BALL_SIZE = 10
PLAYER_SIZE = 20
WIDTH = 800
HEIGHT = 400
GOAL_SIZE = 0.4   #of height
GOAL_DEPTH = 0.05   #of width

#movement
PLAYER_FRICTION = 0.2
BALL_FRICTION = 0.02
MAX_ACCEL = 0.5
SHOOT_DISTANCE = 10
SHOOT_FORCE = 5
COLLISION_FORCE = 0.5

#timing
STEP_TIMING = 20
