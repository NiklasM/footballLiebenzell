#!/usr/bin/env python3.6
# coding=utf-8
import socket

import time

from settings import *


class HumanClient:
    def __init__(self, up :str, down :str, right :str, left :str, kick :str):
        self.socket = None
        
        self.upKey = up
        self.downKey = down
        self.rightKey = right
        self.leftKey = left
        self.kickKey = kick
        
        self.upPressed = False
        self.downPressed = False
        self.rightPressed = False
        self.leftPressed = False
        
        
    def start(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect(('', PORT))
        self.socket.send('{"event":"connect"}\n'.encode("utf-8"))
        
    
    def sendMovement(self):
        ax = 0
        ay = 0
        if self.upPressed: ay -= 1
        if self.downPressed: ay += 1
        if self.rightPressed: ax += 1
        if self.leftPressed: ax -= 1
        if self.socket is not None:
            self.socket.send(('{"event":"accel", "ax":'+str(ax)+', "ay":'+str(ay)+'}\n').encode("utf-8"))

    def onKeyPress(self, event):
        if str(event.keysym) == self.upKey:
            self.upPressed = True
        if event.keysym == self.downKey:
            self.downPressed = True
        if event.keysym == self.rightKey:
            self.rightPressed = True
        if event.keysym == self.leftKey:
            self.leftPressed = True
        if event.keysym == self.kickKey:
            if self.socket is not None:
                self.socket.send('{"event":"kick"}\n'.encode("utf-8"))
        self.sendMovement()
        
    def onKeyUp(self, event):
        if event.keysym == self.upKey:
            self.upPressed = False
        if event.keysym == self.downKey:
            self.downPressed = False
        if event.keysym == self.rightKey:
            self.rightPressed = False
        if event.keysym == self.leftKey:
            self.leftPressed = False
        self.sendMovement()




def simpleClient():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('', PORT))
    s.send('{"event":"connect"}\n'.encode("utf-8"))
    #s.send('{"event":"accel", "ax":1, "ay":0}\n'.encode("utf-8"))
    while True:
        time.sleep(1)
        s.send('{"event":"kick"}\n'.encode("utf-8"))




