#!/usr/bin/env python3.6
# coding=utf-8

class Transformation:
    def __init__(self, addx=0, addy=0, scalex=0, scaley=0):
        self.addx = addx
        self.addy = addy
        self.scalex = scalex
        self.scaley = scaley
        
    
    

class Vector:
    def __init__(self, x :float, y :float):
        self.x = x
        self.y = y
        
    def __add__(self, other):
        return Vector(self.x +other.x, self.y +other.y)
    
    def __mul__(self, scalar :float):
        return Vector(self.x *scalar, self.y *scalar)

    def normalize(self):
        newVector = Vector(self.x, self.y)
        length = newVector.length()
        newVector.x /= length
        newVector.y /= length
        return newVector
    
    def transform(self, transformation :Transformation):
        newVector = Vector(self.x, self.y)
        newVector.x *= transformation.scalex
        newVector.y *= transformation.scaley
        return newVector
    
    def cut(self):
        length2 = self.length2()
        if length2 > 1:
            return self.normalize()
        return Vector(self.x, self.y)
        
    
    def length2(self):
        return self.x*self.x +self.y*self.y
    
    def length(self):
        return self.length2() ** 0.5
    
    @classmethod
    def byJSON(cls, entries):
        v = cls(0, 0)
        v.__dict__.update(entries)
        return v
    
    def reprJSON(self):
        return self.__dict__.copy()


class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y
    
    def __add__(self, other: Vector):
        return Point(self.x + other.x, self.y + other.y)
    
    def __sub__(self, other):
        if isinstance(other, Vector):
            return Point(self.x - other.x, self.y - other.y)
        else:
            return Vector(self.x - other.x, self.y - other.y)
    
    def __mul__(self, scalar: float):
        return Point(self.x * scalar, self.y * scalar)

    def transform(self, transformation :Transformation):
        newPoint = Point(self.x, self.y)
        newPoint.x *= transformation.scalex
        newPoint.y *= transformation.scaley
        newPoint.x += transformation.addx
        newPoint.y += transformation.addy
        return newPoint

    @classmethod
    def byJSON(cls, entries):
        v = cls(0, 0)
        v.__dict__.update(entries)
        return v
    
    def reprJSON(self):
        return self.__dict__.copy()
    